var canvas = document.getElementById("myCanvas");
var pencil = document.getElementById("myPencil");
var erase = document.getElementById("myEraser");
var refresh = document.getElementById("myReset");
/*brushsize*/
var brushSize = document.getElementById("myBrushsize");
var widthOne = document.getElementById("myWidthOne");
var widthFive = document.getElementById("myWidthFive");
var widthTen = document.getElementById("myWidthTen");
var widthFifteen = document.getElementById("myWidthFifteen");
/*color*/
var pickColor = document.getElementById("myColor");
var colorWell = document.getElementById("myColorWell");
/*text*/
var writeText = document.getElementById("myText");
var typeFace = document.getElementById("myTypeface");
var fonts = document.getElementById("chooseTypeface");
var textSize = document.getElementById("myFontsize");
var sizeTen = document.getElementById("mySizeTen");
var sizeTwenty = document.getElementById("mySizeTwenty");
var sizeThirty = document.getElementById("mySizeThirty");
var sizeForty = document.getElementById("mySizeForty");
var sizeFifty = document.getElementById("mySizeFifty");
/*shapes*/
var rectangle = document.getElementById("myRectangle");
var circle = document.getElementById("myCircle");
var triangle = document.getElementById("myTriangle");
/*upload image*/
var upimg = document.getElementById("uploadfile");
var uploadicon = document.getElementById("myImage");
/*undo redo*/
var undo = document.getElementById("myUndo");
var redo = document.getElementById("myRedo");


var ctx = canvas.getContext("2d");
ctx.fillStyle = "#ffffff";
ctx.fillRect(0, 0, 600, 520);
ctx.lineWidth = 5;

var x, y;
var isTyping = false, isRec = false, isCir = false, isTri = false, isImg = false;
var Recfirstpoint = false, recX, recY;
var Cirfirstpoint = false, cirX, cirY, cirMidX, cirMidY, rad;
var Tripoint = 0;
var nowFontFamily = "sans-serif", nowFontSize = "20px ";
var img = new Image(), ratioW, ratioH, finalW, finalH;
var picsArray = [], picsNow = -1;

addStep();

/*functions*/
function getMousePos(canvas,evt) {
	var rect = canvas.getBoundingClientRect();
	return {
		x: evt.clientX - rect.left,
		y: evt.clientY - rect.top
	};
};

function draw(evt) {
	var mousePos = getMousePos(canvas,evt);
	ctx.lineTo(mousePos.x,mousePos.y);
	ctx.stroke();
}

function addStep() {
	picsNow++;
	if(picsNow<picsArray.length) {
		picsArray.length = picsNow;
	}
	picsArray.push(canvas.toDataURL());
}

/*A bunch of addEventListeners*/
canvas.addEventListener("mousedown",function(evt) {
	var mousePos = getMousePos(canvas,evt);
	if(isTyping===true){
		ctx.fillStyle = colorWell.value;
		ctx.fillText(document.getElementById("myInputText").value,mousePos.x,mousePos.y);
	}else if(isRec===true) {
		if(Recfirstpoint===false){
			recX = mousePos.x;
			recY = mousePos.y;
			Recfirstpoint = true;
		}else {
			ctx.strokeStyle = colorWell.value;
			ctx.beginPath();
			ctx.rect(recX,recY,mousePos.x-recX-5,mousePos.y-recY-5);
			ctx.stroke();
			Recfirstpoint = false;
		}
	}else if(isCir===true) {
		if(Cirfirstpoint===false) {
			cirX = mousePos.x;
			cirY = mousePos.y;
			Cirfirstpoint = true;
		}else {
			ctx.strokeStyle = colorWell.value;
			ctx.beginPath();
			if(mousePos.x>cirX){
				cirMidX = cirX + (mousePos.x-cirX)/2;
				cirMidY = cirY + (mousePos.y-cirY)/2;
				rad = Math.sqrt(Math.pow(cirMidX-cirX,2)+Math.pow(cirMidY-cirY,2));
			}else {
				cirMidX = mousePos.x + (cirX-mousePos.x)/2;
				cirMidY = mousePos.y + (cirY-mousePos.y)/2;
				rad = Math.sqrt(Math.pow(cirMidX-mousePos.x,2)+Math.pow(cirMidY-mousePos.y,2));
			}
			ctx.arc(cirMidX,cirMidY,rad,0,2*Math.PI);
			ctx.stroke();
			Cirfirstpoint = false;
		}
	}else if(isTri===true){
		if(Tripoint===0) {
			ctx.strokeStyle = colorWell.value;
			ctx.beginPath();
			ctx.moveTo(mousePos.x,mousePos.y);
			Tripoint = 1;
		}else if(Tripoint===1) {
			ctx.lineTo(mousePos.x,mousePos.y);
			Tripoint = 2;
		}else {
			ctx.lineTo(mousePos.x,mousePos.y);
			ctx.closePath();
			ctx.stroke();
			Tripoint = 0;
		}
	}else if(isImg===true) {
		finalW = img.width;
		finalH = img.height;
		if(img.width>600){
			ratioW = img.width/600 + 1;
			finalW = img.width/ratioW;
			finalH = img.height/ratioW;
		}
		if(finalH>520) {
			ratioH = img.height/520 + 1;
			finalW = img.width/ratioH;
			finalH = img.height/ratioH;
		}
        ctx.drawImage(img,mousePos.x,mousePos.y,finalW,finalH);
	}else {
		ctx.beginPath();
		ctx.moveTo(mousePos.x,mousePos.y);
		canvas.addEventListener("mousemove",draw,false);
	}
});

canvas.addEventListener("mouseup",function() {
	canvas.removeEventListener("mousemove",draw,false);
	if(Recfirstpoint===false&&Cirfirstpoint===false&&Tripoint===0) {
		addStep();
	}
});

pencil.addEventListener("click",function() {
	ctx.strokeStyle = colorWell.value;
	document.body.style.cursor = "url('images/Pencil.cur'),auto";
	isTyping = false;
	isRec = false;
	isCir = false;
	isTri = false;
	isImg = false;
});

erase.addEventListener("click",function() {
	ctx.strokeStyle = "white";
	document.body.style.cursor = "url('images/Eraser.cur'),auto";
	isTyping = false;
	isRec = false;
	isCir = false;
	isTri = false;
	isImg = false;
});

refresh.addEventListener("click",function() {
	ctx.fillStyle = "#ffffff";
	ctx.fillRect(0, 0, 600, 520);
	addStep();
});

/*choose bursh size*/
brushSize.addEventListener("click",function() {
	document.getElementById("showsize").style.display = "block";
	document.getElementById("showcolor").style.display = "none";
});

widthOne.addEventListener("click",function() {
	ctx.lineWidth = 1;
});

widthFive.addEventListener("click",function() {
	ctx.lineWidth = 5;
});

widthTen.addEventListener("click",function() {
	ctx.lineWidth = 10;
});

widthFifteen.addEventListener("click",function() {
	ctx.lineWidth = 15;
});

/*choose color*/
pickColor.addEventListener("click",function() {
	document.getElementById("showcolor").style.display = "block";
	document.getElementById("showsize").style.display = "none";
});

colorWell.addEventListener("change",function(evt) {
	ctx.strokeStyle = evt.target.value;
	if(isRec===true) {
		document.body.style.cursor = "crosshair";
	}else if(isTyping===true) {
		document.body.style.cursor = "url('images/Text.cur'),auto";
	}else if(isCir===true){
		document.body.style.cursor = "url('images/Circle.cur'),auto";
	}else if(isTri===true) {
		document.body.style.cursor = "url('images/Triangle.cur'),auto";
	}else if(isImg===true) {
		document.body.style.cursor = "url('images/Pic.cur'),auto";
	}else {
		document.body.style.cursor = "url('images/Pencil.cur'),auto";
	}
});

/*write text*/
writeText.addEventListener("click",function() {
	document.getElementById("showinput").style.display = "block";
	document.getElementById("showtypeface").style.display = "none";
	document.getElementById("showfontsize").style.display = "none";
	document.body.style.cursor = "url('images/Text.cur'),auto";
	ctx.font = nowFontSize + nowFontFamily;
	isTyping = true;
	isRec = false;
	isCir = false;
	isTri = false;
	isImg = false;
});

typeFace.addEventListener("click",function() {
	document.getElementById("showtypeface").style.display = "block";
	document.getElementById("showinput").style.display = "none";
	document.getElementById("showfontsize").style.display = "none";
});

textSize.addEventListener("click",function() {
	document.getElementById("showfontsize").style.display = "block";
	document.getElementById("showtypeface").style.display = "none";
	document.getElementById("showinput").style.display = "none";
});

fonts.addEventListener("change",function(evt) {
	nowFontFamily = fonts.value;
	ctx.font = nowFontSize + nowFontFamily;
});

sizeTen.addEventListener("click",function() {
	nowFontSize = "10px ";
	ctx.font = nowFontSize + nowFontFamily;
});

sizeTwenty.addEventListener("click",function() {
	nowFontSize = "20px ";
	ctx.font = nowFontSize + nowFontFamily;
});

sizeThirty.addEventListener("click",function() {
	nowFontSize = "30px ";
	ctx.font = nowFontSize + nowFontFamily;
});

sizeForty.addEventListener("click",function() {
	nowFontSize = "40px ";
	ctx.font = nowFontSize + nowFontFamily;
});

sizeFifty.addEventListener("click",function() {
	nowFontSize = "50px ";
	ctx.font = nowFontSize + nowFontFamily;
});

/*shapes*/
rectangle.addEventListener("click",function() {
	isRec = true;
	isTyping = false;
	isCir = false;
	isTri = false;
	isImg = false;
	document.body.style.cursor = "crosshair";
	document.getElementById("showcirrec").style.display = "block";
	document.getElementById("showtriangle").style.display = "none";
});

circle.addEventListener("click",function() {
	isCir = true;
	isRec = false;
	isTyping = false;
	isTri = false;
	isImg = false;
	document.body.style.cursor = "url('images/Circle.cur'),auto";
	document.getElementById("showcirrec").style.display = "block";
	document.getElementById("showtriangle").style.display = "none";
});

triangle.addEventListener("click", function() {
	isTri = true;
	isRec = false;
	isCir = false;
	isTyping = false;
	isImg = false;
	document.getElementById("showtriangle").style.display = "block";
	document.getElementById("showcirrec").style.display = "none";
	document.body.style.cursor = "url('images/Triangle.cur'),auto";
});

/*upload image*/
upimg.addEventListener("change",function(evt) {
	isImg = true;
	isTri = false;
	isRec = false;
	isCir = false;
	isTyping = false;
	var reader = new FileReader();
    reader.onload = function(event){
		img.src = event.target.result;
    }
    reader.readAsDataURL(evt.target.files[0]);
});

uploadicon.addEventListener("click",function() {
	document.body.style.cursor = "url('images/Pic.cur'),auto";
});

/*undo redo*/
undo.addEventListener("click",function() {
	if(picsNow>0) {
		picsNow--;
		Recfirstpoint = false;
		Cirfirstpoint = false;
		Tripoint = 0;
		var showPic = new Image();
		showPic.src = picsArray[picsNow];
		showPic.onload = function() {
			ctx.drawImage(showPic,0,0);
		}
	}
});

redo.addEventListener("click",function() {
	if(picsNow<picsArray.length-1) {
		picsNow++;
		Recfirstpoint = false;
		Cirfirstpoint = false;
		Tripoint = 0;
		var showPic = new Image();
		showPic.src = picsArray[picsNow];
		showPic.onload = function() {
			ctx.drawImage(showPic,0,0);
		}
	}
});