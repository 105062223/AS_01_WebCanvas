# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

# Report
## **其他功能**
1. 當視窗小於1100px時按鈕們會從右側移到畫布的下面

## **主要架構** 
1. 將canvas設為白底(原本是透明的)，並儲存目前的canvas
2. 所有事件的偵測都是使用```addEventListener```
3. 畫在畫布上的動作都是使用```canvas.addEventListener("mousedown",function(evt) {})```去執行，利用isTyping, isRec等變數來判斷目前要執行的功能。
4. 使用```getMousePos```來取得滑鼠在canvas上的位置(要減去canvas左上的座標來取得相對位置)
5. 其他值的設定都是使用```addEventListener("click",function(){})```去偵測

## **細部架構**
1. **Pencil**: 使用"getMousePos"取得滑鼠位置以及"draw"來畫畫，在同時mousedown且mouseover時才會執行"draw"，mouseup時必須把"draw" remove

    ```javascript
    function draw(evt) {
	    var mousePos = getMousePos(canvas,evt);
	    ctx.lineTo(mousePos.x,mousePos.y);
	    ctx.stroke();
    }
    ```

    由於同時mousedown且mouseover時會一直呼叫"draw"，所以在"draw"裡要不斷更新滑鼠目前的位置並使用"lineTo()"連接起來，ctx.beginPath()以及ctx.moveTo()則是放在mousedown偵測裡(一開始點下時執行就好)

    ```javascript
    pencil.addEventListener("click",function() {
	    ctx.strokeStyle = colorWell.value;
	    document.body.style.cursor = "url('images/Pencil.cur'),auto";
	    isTyping = false;
	    isRec = false;
	    isCir = false;
	    isTri = false;
	    isImg = false;
    });
    ```
    當使用者選擇Pencil時，將strokeStyle的值設成和colorwell一樣，並更換cursor icon，同時將其他功能設成false
2. **Eraser**: 和Pencil用到一樣的functions，只不過顏色永遠是白色的
3. **Color**: 這裡是使用```<input type="color">```去實作，使用者改變顏色時要使用```addEventListener("change",function(){})```去偵測，並改變strokeStyle的值
     * 這裡要注意的是如果使用者目前使用的功能是橡皮擦，改變完顏色後橡皮擦會畫出顏色，所以選顏色的同時將cursor icon變成Pencil的icon(若使用者目前的功能是圓形就將cursor icon維持圓形的icon，以此類推)
4. **Brush Size**: 點下Brush Size按鈕後會出現選單，再利用```addEventListener("click",function(){})```偵測使用者點下哪個按鈕，改變lineWidth的值(改變lineWidth會同時改變到圓形、三角形、長方形的粗細)

    ```javascript
    widthOne.addEventListener("click",function() {
	    ctx.lineWidth = 1;
    });
    ```
5. **Circle**: 畫圓的方式為在canvas上點兩次決定直徑的大小
    * Cirfirstpoint: 判斷使用者點了幾次
    * cirX, cirY: 紀錄第一個點的座標
    * cirMidX, cirMidY: 紀錄兩個點的中點，第二個點在第一個點的左上或右下方會影響計算
    * rad: 紀錄半徑
    下面是在mousedown裡執行的部分

    ```javascript
    else if(isCir===true) {
		if(Cirfirstpoint===false) {
			cirX = mousePos.x;
			cirY = mousePos.y;
			Cirfirstpoint = true;
		}else {
			ctx.strokeStyle = colorWell.value;
			ctx.beginPath();
			if(mousePos.x>cirX){
				cirMidX = cirX + (mousePos.x-cirX)/2;
				cirMidY = cirY + (mousePos.y-cirY)/2;
				rad = Math.sqrt(Math.pow(cirMidX-cirX,2)+Math.pow(cirMidY-cirY,2));
			}else {
				cirMidX = mousePos.x + (cirX-mousePos.x)/2;
				cirMidY = mousePos.y + (cirY-mousePos.y)/2;
				rad = Math.sqrt(Math.pow(cirMidX-mousePos.x,2)+Math.pow(cirMidY-mousePos.y,2));
			}
			ctx.arc(cirMidX,cirMidY,rad,0,2*Math.PI);
			ctx.stroke();
			Cirfirstpoint = false;
        }
    }
    ```
6. **Triangle**: 畫三角形的方式是在canvas上點出三個頂點
    * Tripoint: 記錄使用者點了幾次
    使用者每點一次就用lineTo()把點連起來，最後再用closePath()以及stroke()畫出圖形

    ```javascript
    else if(isTri===true){
		if(Tripoint===0) {
			ctx.strokeStyle = colorWell.value;
			ctx.beginPath();
			ctx.moveTo(mousePos.x,mousePos.y);
			Tripoint = 1;
		}else if(Tripoint===1) {
			ctx.lineTo(mousePos.x,mousePos.y);
			Tripoint = 2;
		}else {
			ctx.lineTo(mousePos.x,mousePos.y);
			ctx.closePath();
			ctx.stroke();
			Tripoint = 0;
		}
	}
    ```
7. **Rectangle**: 畫長方形的方式是在canvas上點出左上和右下的點，在使用rect()時要多減5是因為我把canvas的border設為5px
    * Recfirstpoint: 判斷使用者點了幾次
    * recX, recY: 紀錄第一個點的位置

    ```javascript
    else if(isRec===true) {
		if(Recfirstpoint===false){
			recX = mousePos.x;
			recY = mousePos.y;
			Recfirstpoint = true;
		}else {
			ctx.strokeStyle = colorWell.value;
			ctx.beginPath();
			ctx.rect(recX,recY,mousePos.x-recX-5,mousePos.y-recY-5);
			ctx.stroke();
			Recfirstpoint = false;
		}
	}
    ```
8. **Text**: 使用方式是先在```<input type="text">```裡輸入文字之後在canvas上點出文字，文字的顏色也是從Color選取(設定fillstyle的值)。點選Text時會改變cursor icon並將isTyping設為true(其他功能為false)

    ```javascript
    if(isTyping===true){
		ctx.fillStyle = colorWell.value;
		ctx.fillText(document.getElementById("myInputText").value,mousePos.x,mousePos.y);
	}
    ```
9. **Typeface & Font Size**: 點選"Typeface"後會出現下拉選單提供選項(這裡設定了五個)，點選"Font Size"會出現像Brush size一樣的選單
    * nowFontSize, nowFontFamily: 紀錄目前使用的值

    ```javascript
    fonts.addEventListener("change",function(evt) {
	    nowFontFamily = fonts.value;
	    ctx.font = nowFontSize + nowFontFamily;
    });

    sizeTen.addEventListener("click",function() {
	    nowFontSize = "10px ";
	    ctx.font = nowFontSize + nowFontFamily;
    });
    ```
10. **Undo & Redo**: 使用picsArray來儲存每個步驟的canvas，當每次mouseup或者按下Reset就要儲存一次，mouseup部分要特別判斷如果使用者畫圖形時還沒點完就先不要儲存(因為圖形設計是全部點完才會在畫布顯示)
    * **addStep()**: 呼叫這個function代表要儲存canvas，每呼叫一次將picsNow+1，判斷如果picsNow小於陣列長度代表undo後的新canvas，要將之前的canvas覆蓋掉，所以陣列長度要跟picsNow一樣，利用```canvas.toDataURL()```儲存canvas
    * **undo.addEventListener("click",function(){})**: 每次undo要將picsNow-1(除非目前是最初的canvas)並用drawImage畫上之前的canvas，要使用onload()否則畫不出圖片
    * **redo.addEventListener("click",function(){})**: 每次redo要將picsNow+1(除非目前是最新的canvas)並用drawImage畫上canvas
11. **Reset**: 利用fillStyle以及fillRect將canvas設為全白
12. **Image**: 利用```<input type="file">```以及filereader讓使用者能從自己的電腦上傳圖片，因為想用自己的圖片取代原本的按鈕所以用了```<label for="">```，選擇好圖片後在canvas上點選要放圖片的位置，由於大部分圖片都比canvas大，所以較大的圖片會照比例縮小

    ```javascript
    else if(isImg===true) {
		finalW = img.width;
		finalH = img.height;
		if(img.width>600){
			ratioW = img.width/600 + 1;
			finalW = img.width/ratioW;
			finalH = img.height/ratioW;
		}
		if(finalH>520) {
			ratioH = img.height/520 + 1;
			finalW = img.width/ratioH;
			finalH = img.height/ratioH;
		}
        ctx.drawImage(img,mousePos.x,mousePos.y,finalW,finalH);
	}
    ```
    一開始先判斷圖片的寬度有沒有比canvas寬，有的話先照比例縮小一遍，要是高度還是超過再縮小一遍，比例的計算單純是看圖片是canvas的幾倍再加上1，加上1的原因是若只縮成和canvas一樣寬，使用者必須在canvas最左邊點擊才能把整張圖片放進canvas
13. **Download**: 使用```<a download="image.png">```下載圖片

## **遇到的問題**
1. 一開始將```<script>```放在```<head>```裡，造成canvas element還沒跑好javascript就想取得access，解決方法是放在```<body>```的最下面
2. 原本都是在html的element後面加上onmousedown、onclick等來呼叫function，但是這樣做不出同時判斷mousedown和mousemove的效果，上網查了一下才都改用addEventListener，這樣html也比較乾淨(不過addEventListener就暴增)
3. 原本都沒有使用ctx.beginPath()，直到有次橡皮擦擦完換回畫筆時發現橡皮擦的路徑從白色變成有顏色才發現
4. 圖形、文字以及上傳的圖片原本都想要可以讓使用者拖曳或是改變大小，但這樣做的話會需要一直重畫canvas造成之前畫的東西會被刷掉，到最後想不到怎麼做所以都改成比較簡單的作法